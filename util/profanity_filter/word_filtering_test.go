/*
 * Name : Okto Prima Jaya
 * GitHub : https://github.com/oktopriima
 * Email : octoprima93@gmail.com
 * Created At : 15/06/23, 09:48
 * Copyright (c) 2023
 */

package profanity_filter

import "testing"

func TestWordFiltering_Filter(t *testing.T) {
	type fields struct {
		Censored []string
	}
	type args struct {
		sentences string
	}
	tests := []struct {
		name   string
		fields fields
		args   args
	}{
		{
			name: "positive case 1 using wildcards",
			fields: fields{Censored: []string{
				"*fuck*",
			}},
			args: args{sentences: "This is so fucking awesome motherfucker fuck fucker"},
		},
		{
			name: "positive case 2 without wildcards",
			fields: fields{Censored: []string{
				"fuck",
				"fucker",
				"fucking",
			}},
			args: args{sentences: "This is so fucking awesome motherfucker fuck fucker"},
		},
		{
			name: "negative case 1 not matches without wildcard",
			fields: fields{Censored: []string{
				"cats",
			}},
			args: args{sentences: "This is so fucking awesome motherfucker fuck fucker"},
		},
		{
			name: "negative case 2 not matches with wildcard",
			fields: fields{Censored: []string{
				"*cat",
			}},
			args: args{sentences: "This is so fucking awesome motherfucker fuck fucker"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			f := NewWordFiltering(tt.fields.Censored)

			got := f.Filter(tt.args.sentences)
			t.Logf("New sentences: %s", got)
		})
	}
}
