package profanity_filter

import (
	"context"
	"encoding/json"
	"fmt"
	"net/http"
)

type client struct {
	Url        string
	httpClient *http.Client
}

type Client interface {
	GetUserProfanitySetting(ctx context.Context, token string) (*UserProfanityFilterResponse, error)
	GetBadWordsList(ctx context.Context) (*StandardBadWordList, error)
}

func NewClient(Url string) Client {
	return &client{
		Url:        Url,
		httpClient: &http.Client{},
	}
}

func (c *client) GetUserProfanitySetting(ctx context.Context, token string) (*UserProfanityFilterResponse, error) {
	url := fmt.Sprintf("http://%s/api/users/profanity-filter", c.Url)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}
	req.Header.Add("Authorization", token)

	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed make request. error : %s", res.Status)
	}

	output := new(UserProfanityFilterResponse)
	err = json.NewDecoder(res.Body).Decode(&output)
	if err != nil {
		return nil, err
	}

	return output, nil
}

func (c *client) GetBadWordsList(ctx context.Context) (*StandardBadWordList, error) {
	url := fmt.Sprintf("http://%s/api/users/profanity-filter/standard-bad-word-list", c.Url)
	req, err := http.NewRequestWithContext(ctx, http.MethodGet, url, nil)
	if err != nil {
		return nil, err
	}

	res, err := c.httpClient.Do(req)
	if err != nil {
		return nil, err
	}

	defer res.Body.Close()

	if res.StatusCode != http.StatusOK {
		return nil, fmt.Errorf("failed make request. error : %s", res.Status)
	}

	output := new(StandardBadWordList)
	err = json.NewDecoder(res.Body).Decode(&output)
	if err != nil {
		return nil, err
	}

	return output, nil
}
