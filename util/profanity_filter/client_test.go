package profanity_filter

import (
	"context"
	"net/http"
	"testing"
)

func Test_client_GetUserProfanitySetting(t *testing.T) {
	type fields struct {
		Url        string
		httpClient *http.Client
	}
	type args struct {
		ctx   context.Context
		token string
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "http get user profanity filter settings",
			fields: fields{
				Url: "api-staging.ganknow.com",
			},
			args: args{
				ctx:   context.Background(),
				token: "Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJhdXRob3JpemVkRGV2aWNlIjoiTW96aWxsYS81LjAgKE1hY2ludG9zaDsgSW50ZWwgTWFjIE9TIFggMTBfMTVfNykgQXBwbGVXZWJLaXQvNTM3LjM2IChLSFRNTCwgbGlrZSBHZWNrbykgQ2hyb21lLzExMy4wLjAuMCBTYWZhcmkvNTM3LjM2IiwiYXV0aG9yaXplZElwdjQiOiIxNDkuMTEzLjEyMS4yMDQsIDEyNy4wLjAuMSIsImV4cCI6MTcxNzU3OTUwOSwiZXhwaXJlZEF0IjoiMjAyNC0wNi0wNVQwOToyNTowOS41ODMwNzQ2M1oiLCJpc0xvbmdMaXZlIjp0cnVlLCJsYXN0RW1haWxVcGRhdGUiOjE2ODMwMjg5MzEsImxhc3RQYXNzd29yZFVwZGF0ZSI6MTY3OTI5Mzk2Niwicm9sZSI6InVzZXJzLUV1VzJPbS1jQUlUaXQiLCJ0b2tlbklEIjoiMjAxMjNlMGUtNGU3My00NmM0LWJkYTQtYzZjMDNjM2YzYzkxIiwidXNlcklEIjoiMTg0MzliOWUtM2Q3My00MzdjLThlOTQtZGRkYjQxYWI2MjcxIn0.I5NeXzsG6fWdrucKu_UAL-2Ajmh-z874JVPXzN49eCA",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewClient(tt.fields.Url)

			got, err := c.GetUserProfanitySetting(tt.args.ctx, tt.args.token)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetUserProfanitySetting() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Logf("Got : %v", got)
		})
	}
}

func Test_client_GetBadWordsList(t *testing.T) {
	type fields struct {
		Url        string
		httpClient *http.Client
	}
	type args struct {
		ctx context.Context
	}
	tests := []struct {
		name    string
		fields  fields
		args    args
		wantErr bool
	}{
		{
			name: "get standard bad word list",
			fields: fields{
				Url: "api-staging.ganknow.com",
			},
			args: args{
				ctx: context.Background(),
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			c := NewClient(tt.fields.Url)
			got, err := c.GetBadWordsList(tt.args.ctx)
			if (err != nil) != tt.wantErr {
				t.Errorf("GetBadWordsList() error = %v, wantErr %v", err, tt.wantErr)
				return
			}
			t.Logf("Got %v", got)
		})
	}
}
