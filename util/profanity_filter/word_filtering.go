/*
 * Name : Okto Prima Jaya
 * GitHub : https://github.com/oktopriima
 * Email : octoprima93@gmail.com
 * Created At : 15/06/23, 09:38
 * Copyright (c) 2023
 */

package profanity_filter

import (
	"strings"
)

type WordFiltering struct {
	Censored []string
}

func NewWordFiltering(censored []string) WordFiltering {
	return WordFiltering{
		Censored: censored,
	}
}

func (f WordFiltering) Filter(sentences string) string {
	var newSlice []string
	if len(f.Censored) <= 0 {
		return sentences
	}

	// convert str into a slice
	strSlice := strings.Fields(sentences)

	for position, word := range strSlice {
		for _, forbiddenWord := range f.Censored {
			// check wild match word
			if strings.Contains(forbiddenWord, "*") {
				m := NewWildMatch(forbiddenWord)
				if m.Matches(word) {
					replacement := strings.Repeat("*", 3)

					strSlice[position] = replacement
					newSlice = append(strSlice[:position], strSlice[position:]...)
				}
			} else {
				if i := strings.Index(strings.ToLower(word), forbiddenWord); i > -1 {
					replacement := strings.Repeat("*", 3)

					strSlice[position] = replacement
					newSlice = append(strSlice[:position], strSlice[position:]...)
				}
			}
		}
	}

	newSentences := strings.Join(newSlice, " ")
	if len(newSentences) <= 0 {
		newSentences = sentences
	}

	return newSentences
}
