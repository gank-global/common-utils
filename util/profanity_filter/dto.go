package profanity_filter

type UserProfanityFilterResponse struct {
	Data struct {
		Id                    string   `json:"id"`
		UserId                string   `json:"userId"`
		FilterMode            string   `json:"filterMode"`
		IsEnabled             bool     `json:"isEnabled"`
		EnableStandardBadWord bool     `json:"enableStandardBadWord"`
		EnableCustomBadWord   bool     `json:"enableCustomBadWord"`
		CustomBadWordList     []string `json:"customBadWordList"`
	} `json:"data"`
	Total int `json:"total"`
}

type StandardBadWordList struct {
	Data struct {
		Words []string `json:"words"`
	} `json:"data"`
	Total int `json:"total"`
}
