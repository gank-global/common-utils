module bitbucket.org/gank-global/common-utils

go 1.15

require (
	github.com/DataDog/sketches-go v0.0.0-20190923095040-43f19ad77ff7 // indirect
	github.com/benbjohnson/clock v1.0.0 // indirect
	github.com/best-expendables/logger v0.0.0-20200511084842-8247cf6c59bd
	github.com/best-expendables/newrelic-context v0.0.0-20210228071950-46ffa3a928cc
	github.com/best-expendables/trace v0.0.0-20200511055751-fb29d033fd2d // indirect
	github.com/best-expendables/user-service-client v0.0.0-20200511060456-3fcf8ea240f5 // indirect
	github.com/fatih/structs v1.1.0
	github.com/go-chi/chi v4.1.2+incompatible
	github.com/go-playground/universal-translator v0.17.0 // indirect
	github.com/go-redis/cache/v8 v8.1.1
	github.com/go-redis/redis/v8 v8.3.1
	github.com/gofrs/uuid v3.3.0+incompatible
	github.com/google/gofuzz v1.0.0 // indirect
	github.com/gorilla/schema v1.2.0
	github.com/jinzhu/gorm v1.9.12 // indirect
	github.com/leodido/go-urn v1.2.0 // indirect
	github.com/newrelic/go-agent v3.4.0+incompatible // indirect
	github.com/opentracing/opentracing-go v1.1.1-0.20190913142402-a7454ce5950e // indirect
	github.com/patrickmn/go-cache v2.1.0+incompatible
	github.com/pkg/errors v0.9.1
	github.com/sirupsen/logrus v1.8.0 // indirect
	github.com/yuin/gopher-lua v0.0.0-20191220021717-ab39c6098bdb // indirect
	google.golang.org/grpc v1.29.1 // indirect
	gopkg.in/go-playground/assert.v1 v1.2.1 // indirect
	gopkg.in/go-playground/validator.v9 v9.31.0
	gopkg.in/redis.v5 v5.2.9 // indirect
	gorm.io/gorm v1.20.2
)
